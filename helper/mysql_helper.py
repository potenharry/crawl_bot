# -*- coding: utf-8 -*-
from mysql.connector import pooling
from mysql.connector.cursor import MySQLCursorPrepared
import util.my_logger as log
import mysql.connector
import MySQLdb
import config.mysql_config as db_config
import datetime

format_msg = '[%(levelname)s]%(asctime)s > %(message)s'
logger = log.CustomLogger.__call__('info', format_msg)

db_infos = {
    'bigblue': {'host': db_config.db1['host'], 'port': db_config.db1['port'],
            'user': db_config.db1['user'], 'passwd': db_config.db1['passwd'], 'db': db_config.db1['db']},
    'dev3': {'host': db_config.db2['host'], 'port': db_config.db2['port'],
            'user': db_config.db2['user'], 'passwd': db_config.db2['passwd'], 'db': db_config.db2['db']},
    'supply': {'host': db_config.db3['host'], 'port': db_config.db3['port'],
            'user': db_config.db3['user'], 'passwd': db_config.db3['passwd'], 'db': db_config.db3['db']},
    'real': {'host': db_config.db4['host'], 'port': db_config.db4['port'],
            'user': db_config.db4['user'], 'passwd': db_config.db4['passwd'], 'db': db_config.db4['db']}
}

artice_table = 'article_set'


def get_connect():
    db_info = db_infos['supply']
    db = MySQLdb.connect(host=db_info['host'], port=db_info['port'],
                         user=db_info['user'], passwd=db_info['passwd'], db=db_info['db'], charset='utf8')

    # db.autocommit(True)
    return db


def get_connect_mysql(db_name):
    db_info = db_infos[db_name]
    db = mysql.connector.connect(host=db_info['host'], port=db_info['port'], user=db_info['user'],
                                 passwd=db_info['passwd'], db=db_info['db'], charset='utf8')

    return db


def get_connect_pool():
    db_info = db_infos['bigblue']
    # " Pool size should be higher than 0 and lower or equal to 32 "
    db = pooling.MySQLConnectionPool(pool_name="mysqlpool", pool_size=32, pool_reset_session=True,
                                     host=db_info['host'], port=db_info['port'], user=db_info['user'],
                                     passwd=db_info['passwd'], db=db_info['db'], charset='utf8',
                                     use_pure=True)
    return db


def store(data_set, data_type):
    conn = get_connect_mysql('supply')
    cur_date = str(datetime.datetime.now())[:10]
    if data_type == 'supply':
        for data in data_set:
            print(data)
            execute_mysql_supply(conn, data, cur_date)

    conn.close()


def store_many_once(data_set, data_type):
    conn = get_connect_mysql('real')
    cursor = conn.cursor()

    sql = "INSERT INTO stock_data (code, date, frgn_val, frgn_qty, inst_val, inst_qty, indv_val, indv_qty) " \
          "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"

    sql += " ON DUPLICATE KEY UPDATE " \
           "code=%s, date=%s, frgn_val=%s, frgn_qty=%s, inst_val=%s, inst_qty=%s, indv_val=%s, indv_qty=%s"

    sql_data_set = list()
    cur_date = str(datetime.datetime.now())[:10]
    logger.print_info('mysql data count :' + str(len(data_set)))
    if data_type == 'supply':
        for idx, data in enumerate(data_set):
            # TypeError: 'bool' object is not subscriptable
            try:
                stock_code = 'A' + data['stock_code']
                sql_data_form = (stock_code, cur_date, data['frgn_val'], data['frgn_qty'], data['inst_val'], data['inst_qty'],
                            data['indv_val'], data['indv_qty'],
                            stock_code, cur_date, data['frgn_val'], data['frgn_qty'], data['inst_val'], data['inst_qty'],
                            data['indv_val'], data['indv_qty'])

                sql_data_set.append(sql_data_form)

                if len(data_set) - 1 == idx:
                    logger.print_info('idx: ' + str(idx) + 'length: ' + str(len(data_set)))
                    logger.print_info('here!!' + str(sql_data_set))

                if len(sql_data_set) == 200 or len(data_set) - 1 == idx:  # TODO 200 ~
                    logger.print_info('sql data set' + str(sql_data_set))
                    for sql_data in sql_data_set:
                        print(sql_data)
                        cursor.execute(sql, sql_data)
                    sql_data_set = []

            except Exception as e:
                logger.print_error('[MYSQL DATA ERROR]: ' + str(e))

    logger.print_info('sql set: ' + str(sql_data_set))
    conn.commit()
    cursor.close()
    conn.close()


def execute_mysql_supply(conn, data, cur_date):
    """
        Update 1m data (from get_ohlc_sql function) in MySQL
    """

    stock_code = 'A' + data['stock_code']
    # if (len(sql) > 0 and rh.check_exist_key(main_key) != 1) or '2019.06.24' in date: # 특정 데이터 넣고 싶을때
    sql = "INSERT INTO supply_test3 (code, date, frgn_val, frgn_qty, inst_val, inst_qty, indv_val, indv_qty) " \
          "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"

    sql += " ON DUPLICATE KEY UPDATE "\
           "code=%s, date=%s, frgn_val=%s, frgn_qty=%s, inst_val=%s, inst_qty=%s, indv_val=%s, indv_qty=%s"

    data_set = (stock_code, cur_date, data['frgn_val'], data['frgn_qty'], data['inst_val'], data['inst_qty'],
                data['indv_val'], data['indv_qty'],
                stock_code, cur_date, data['frgn_val'], data['frgn_qty'], data['inst_val'], data['inst_qty'],
                data['indv_val'], data['indv_qty'])

    try:
        print(sql)
        cursor = conn.cursor(cursor_class=MySQLCursorPrepared)
        cursor.execute(sql, data_set)
        conn.commit()
        cursor.close()
        print('cursor is closed')
        print('success')

    except Exception as e:
        print(e)
        return False

    return True


def get_stock_code():
    db = get_connect_mysql('dev3')
    cursor = db.cursor()
    cur_date = str(datetime.datetime.now())[:10]
    sql = "select distinct(code), market from master_data where date='{}';".format(cur_date)  #TODO working_day와 비교후 돌아가게
    # sql = "select distinct(code), market from master_data where date='{}';".format('20190722')

    print('sql:', sql)
    cursor.execute(sql)
    result = cursor.fetchall()
    cursor.close()
    db.close()

    return result


def get_all_stock_supply_set():
    conn = get_connect_mysql('dev3')
    sql = "select code from supply_test5 where date ='20190719'"
    temp_stock_set = get_stock_code()
    cursor = conn.cursor()
    cursor.execute(sql)
    result = cursor.fetchall()
    real_stock_set= list()
    comp_stock_set = list()
    for stock_set in temp_stock_set:
        real_stock_set.append(stock_set[0])
    print(len(real_stock_set), len(result))

    for data in result:
        comp_stock_set.append(data[0])

    for stock in real_stock_set:
        if stock not in comp_stock_set:
            print(stock)


def check_add_stock_data():
    conn = get_connect_mysql('real')
    sql = "select status from task_flow where id = 'add_stock_data'"  # real
    # sql = "select status from task_flow where id = 'test_root_node'"  # test
    cursor = conn.cursor()
    cursor.execute(sql)
    result = cursor.fetchall()
    flag = result[0][0]
    cursor.close()
    conn.close()
    if flag == 'y':
        return True
    else:
        return False

# get_all_stock_supply_set()  # master_data 와 supply_test 데이터 비교
