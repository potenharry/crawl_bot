import util.my_logger as log
import logging

format_msg = '[%(levelname)s] {} %(asctime)s > %(message)s'.format('test.py')
logger = log.CustomLogger.__call__('info', format_msg)


if True:
    logger.print_error('error')