import requests
import helper.mysql_helper as dh
import util.my_logger as log
import util.tgalarm as tg
import util.store_local_file as st
from multiprocessing import Process, current_process, Pool
from urllib3 import HTTPConnectionPool, PoolManager
import time

format_msg = '[%(levelname)s]%(asctime)s > %(message)s'
logger = log.CustomLogger.__call__('info', format_msg)

start_time = time.time()

target_url = 'https://sandbox-apigw.koscom.co.kr/v2/market/stocks/market_code/stock_code/investors'
headers = {'apikey': 'l7xx9bc8fbf9cc9e40eab5211c024a63f36e'}

indv_num = 10
frgn_num = 11
inst_num = 8

'''
    frgn_val, frgn_qty, indv_val, indv_qty, inst_val, inst_qty
'''

http = PoolManager()


def get_api_data(stock_code, market_flag):
    '''
    :param stock_code: 종목코드 , format: 'A005930'
    :param market_flag:  시장코드 (1: 코스피 , 0: 코스닥)
    :return: api로 부터 받은 데이터

    error
    => timeout, toomanyRedirect, RequestException => false 리턴
    '''
    market = ''
    stock_code = stock_code.replace('A', '')
    if market_flag == 1:
        market = 'kospi'
    elif market_flag == 0:
        market = 'kosdaq'

    url = target_url.replace('market_code', market).replace('stock_code', stock_code)

    try:
        logger.print_info(str(stock_code) + ' start!')
        response = requests.get(url, headers=headers, timeout=(10, 30))
        # response = http.request('GET', url, headers=headers, timeout=(10,30))
        # HTTPSConnectionPool(host='sandbox-apigw.koscom.co.kr', port=443): Read timed out. (read timeout=30)
        # print(response)
        data = response.json()

        if 'error' in data:
            err = data['error']
            tg.sendTo('data crawl error: ' + str(err))
            return 'error'
        logger.print_info(str(stock_code) + ' end!')

        return data['result']

    except requests.exceptions.Timeout as e:
        logger.print_error(str(stock_code) + ' timeout! : ' + str(e))
        print(e)
        return False
    except requests.exceptions.TooManyRedirects as e:
        # Tell the user their URL was bad and try a different one
        logger.print_error(str(stock_code) + ' Too Many Redirects! : ' + str(e))
        print(e)
        return False
    except requests.exceptions.RequestException as e:
        # catastrophic error. bail.
        logger.print_error(str(stock_code) + ' RequestException! : ' + str(e))
        print(e)
        return False


def process_data(data):
    '''
    api로 받은 데이터로 개인, 외국인, 기관의 수급동향을 계산하는 함수
    :param data: api로 받은 데이터
    :return: 개인, 외국인, 기관의 수급동향
    '''
    return_data = dict()

    if not data:
        print('data:', data)
        return False

    return_data['stock_code'] = data['isuSrtCd']
    result_data = data['invstLists']
    for result in result_data:
        if result['invstCd'] == '10':  # 개인
            return_data['indv_val'] = round((result['bidTrdval'] - result['askTrdval']) / 1000)
            return_data['indv_qty'] = result['bidTrdvol'] - result['askTrdvol']

        elif result['invstCd'] == '11':  # 외국인
            return_data['frgn_val'] = round((result['bidTrdval'] - result['askTrdval']) / 1000)
            return_data['frgn_qty'] = result['bidTrdvol'] - result['askTrdvol']

        elif result['invstCd'] == '08':  # 기관
            return_data['inst_val'] = round((result['bidTrdval'] - result['askTrdval']) / 1000)
            return_data['inst_qty'] = result['bidTrdvol'] - result['askTrdvol']

    return return_data


def get_process_data(stock_set, return_data):
    failed_stocks = list()
    for stock_code, market in stock_set:
        # @stock_code : str, @market : int
        result = get_api_data(stock_code, market)  #False or error

        if result is 'error':
            logger.print_error('get_process_data error (line 79) : ', str(result))
            continue

        if result is False:
            failed_stocks.append((stock_code, market))
            continue

        processed_data = process_data(result)
        print('process data:', processed_data)
        return_data.append(processed_data)

        if processed_data is False:
            failed_stocks.append((stock_code, market))
            continue

        time.sleep(0.01)

    return return_data, failed_stocks


def get_process_data_use_pool(stock_set):
    '''
    :param stock_set: 종목과 마켓넘버 튜플 , format : ('A005930', 1)
    :return: return_data : 성공한 stock_set 모음 , failed_data : 제대로 받아오지 못한 stock_set 모음
    '''
    failed_stocks = list()
    return_data = list()
    print('stock_set: ', stock_set)
    stock_code, market = stock_set
    # @stock_code : str, @market : int
    result = get_api_data(stock_code, market)
    if result is 'error':
        logger.print_error('get_process_data error (line 117) : ', str(result))
    if result is False:  # timeout 경우
        failed_stocks.append((stock_code, market))
        return False, failed_stocks

    st.store_raw_data(result, 'raw')
    processed_data = process_data(result)
    return_data.append(processed_data)
    if processed_data is False:
        failed_stocks.append((stock_code, market))
    time.sleep(0.01)  # TODO 시간 조절

    return return_data, failed_stocks


def proceed_crawl():
    stock_set = dh.get_stock_code()
    conn = dh.get_connect_mysql()
    conn.close()
    return_data = list()

    while True:  # 실패한 종목이 없을때까지 반복
        return_data, failed_stocks = get_process_data(stock_set, return_data)

        if not failed_stocks:
            break
        else:
            stock_set = failed_stocks

    return return_data, 'supply'


def proceed_crawl_use_pool():
    '''
    main 함수 역할을 함, process를 활용, 2600여개의 종목을 100개씩 5개의 프로세스로 나눠서 진행
    :return: return_data : 각 stock에 대한 수급동향 정보 모음
    'supply' : 수급 정보임을 넘겨줌
    '''
    stock_set = dh.get_stock_code()
    logger.print_info('stock count: ' + str(len(stock_set)))
    return_data = list()
    failed_data_set = list()
    data_set = list()
    for idx, data in enumerate(stock_set):
        data_set.append(data)
        if len(data_set) == 100 or idx == len(stock_set) - 1:
            print('pool start!')
            with Pool(5) as pool:
                for result, failed_data in pool.map(get_process_data_use_pool, data_set):
                    if result is not False:
                        return_data += result
                    failed_data_set += failed_data
            data_set = []

    if failed_data_set:  # failed_data_set을 넘겨 무한루프를 돌며 제대로 데이터가 다 들어갈때까지 반복
        return_data += proceed_crawl_specific_stock(failed_data_set)

    return return_data, 'supply'


def proceed_crawl_specific_stock(stock_set):  # stock_set format : [(stock_code, market) , ...]
    '''
    failed_data_set을 넘겨 받아 무한루프를 돌며 제대로 데이터가 다 들어갈때까지 반복
    :param stock_set: format: [('005930', 1), ....]
    :return:
    '''
    return_data = list()
    while True:  # 실패한 종목이 없을때까지 반복
        return_data, failed_stocks = get_process_data(stock_set, return_data)  # TODO result or return_data

        if not failed_stocks:  # 데이터를 받아오는데 실패한 종목이 없는 경우
            break
        else:  # 실패한 종목들을 가지고 다시 루프를 돈다.
            stock_set = failed_stocks

    return return_data


if __name__ == '__main__':
    proceed_crawl()
