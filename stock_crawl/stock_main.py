import stock_crawl.supply_crawl.supply_crawler as supply_crawler
import helper.mysql_helper as mysql_helper
import util.store_local_file as store_file
import util.post_hadler as post_hadler
import util.tgalarm as tg
import time
import sys
'''
계정 : bankchain
패스워드 : Qodzmcpdls12!!

API 플랜 (https://developers.koscom.co.kr/api-catalog)
할당량 : 50000 hits / day  
속도 제한 : 50 calls /sec 
'''

start_time = time.time()
crawlers = [supply_crawler.proceed_crawl_use_pool]
transporters = [store_file.store, mysql_helper.store_many_once]


def main_function():
    tg.sendTo('dev', 'Task start: supply_data_crawling')
    post_hadler.post_running_to_iqflow('supply_data_crawling')
    for crawler in crawlers:
        result, data_type = crawler()
        for transporter in transporters:
            transporter(result, data_type)

    response_result = post_hadler.post_success_to_iqflow('supply_data_crawling')

    if response_result is False:
        post_hadler.post_success_to_iqflow('supply_data_crawling')

    tg.sendTo('dev', 'Task success: supply_data_crawling')
    print("--- %s seconds ---" % (time.time() - start_time))


def dummy_function():
    print('dummy start ')  # 더미 추가
    time.sleep(60)
    return True


if __name__ == '__main__':
    version = sys.argv[1]  # 'daemon' or 'real'

    if version == 'linux':
        main_function()

    if version == 'windows':
        print('daemon start')
        while True:
            add_stock_is_success = mysql_helper.check_add_stock_data()
            if add_stock_is_success:  # True = 'y' , False = '0'
                print('add_stock_data is success')
                '''
                    test code
                    result = dummy_function()
                    if result:
                        post_hadler.post_success_to_iqflow('test_root_node')
                        tg.sendTo('harry', 'supply success!')
                '''
                main_function()
                break

            else:
                print('add_stock_data가 아직 완료되지 않았습니다.')
                time.sleep(60 * 3)

