import helper.mysql_helper as dh
import sys


def read_file(target_file):
    dir_path = '../logs/'
    result = list()
    with open(dir_path + target_file, 'r') as f:
        while True:
            line = f.readline()
            if not line:
                break
            temp_data = eval(line)
            result.append(temp_data)

    return result


def store_mysql(data_set):
    dh.store_many_once(data_set, 'supply')


if __name__ == '__main__':
    target_file_name = sys.argv[1]
    result = read_file(target_file_name)
    store_mysql(result)
