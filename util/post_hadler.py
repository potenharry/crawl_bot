import requests
import json
'''
주소: http://192.168.0.50:5100/service/iqflow/report
method: POST
헤더: "Content-Type": "application/json"
바디: {"task_id": "작업ID", "result":"success"}
시험용 작업 ID : 	test_root_node
'''


def post_success_to_iqflow(task_id):
    target_url = 'http://192.168.0.50:5100/service/iqflow/report'
    headers = {'Content-Type': 'application/json'}
    body = {"task_id": task_id, "result": "success"}
    response = requests.post(target_url, headers=headers, data=json.dumps(body))
    if response.status_code != 200:
        failed_request(response.status_code)
    else:
        print('post success')


def post_running_to_iqflow(task_id):
    target_url = 'http://192.168.0.50:5100/service/iqflow/report'
    headers = {'Content-Type': 'application/json'}
    body = {"task_id": task_id, "result": "running"}
    response = requests.post(target_url, headers=headers, data=json.dumps(body))
    if response.status_code != 200:
        failed_request(response.status_code)
    else:
        print('post success')


def failed_request(status_code):
    print('Reqeust Failed: ', status_code)
    return False

