from util.tgalarm import *
from logging import handlers
import logging
import os


class SingletonType(type):
    def __call__(cls, *args, **kwargs):
        try:
            return cls.__instance
        except AttributeError:
            cls.__instance = super(SingletonType, cls).__call__(*args, **kwargs)
            return cls.__instance


class CustomLogger(object, metaclass=SingletonType):
    _logger = None

    def __init__(self, level_name, format_msg, dir_name="../logs/", file_name='.log'):
        """
            Singleton class
            Only first time call __init__ method

        """
        self._logger = logging.getLogger()
        self._logger.setLevel(self.set_level(level_name))
        self.formatter = logging.Formatter(format_msg)
        self.dirname = dir_name
        if not os.path.isdir(self.dirname):
            os.mkdir(self.dirname)
        fileHandler = self.set_filehandler(file_name)
        self.add_handler(fileHandler)

    def add_handler(self, handler_name):
        self._logger.addHandler(handler_name)

    def del_handler(self, handler_name):
        self._logger.removeHandler(handler_name)

    def set_filehandler(self, file_name):
        """
            fileHandler's path setting
            default type = "./logs/YYYY-mm-dd HH:MM:SS.log
            possible to change : dir_name(default=./logs/), file_name(default = .log)
        """
        import datetime
        now = datetime.datetime.now()

        file_name = self.dirname + now.strftime("%Y-%m-%d") + file_name
        fileHandler = handlers.TimedRotatingFileHandler(filename=file_name, when='midnight', interval=1,
                                                        encoding='utf-8')
        fileHandler.setFormatter(self.formatter)
        return fileHandler

    def set_streamhandler(self):
        # Add streamHandler, not in __init__ , After get instance
        streamHandler = logging.StreamHandler()
        streamHandler.setFormatter(self.formatter)
        self.add_handler(streamHandler)

    def change_filehandler_format(self, format_msg, handler_name="fileHandler", file_name='.log'):
        """
            change filehandler => change logging.Filehandler type
            Also change formatter
            And renew filehandler
        """
        self.del_handler(handler_name)
        self.change_formatter(format_msg)
        fileHandler = self.set_filehandler(file_name)
        self.add_handler(fileHandler)

    def change_formatter(self, foramt_msg):
        self.formatter = logging.Formatter(foramt_msg)

    def set_level(self, level):
        if level == "debug" or level == "DEBUG":
            return logging.DEBUG
        elif level == "info" or level == "INFO":
            return logging.INFO
        elif level == "error" or level == "ERROR":
            return logging.ERROR
        elif level == "critical" or level == "CRITICAL":
            return logging.CRITICAL
        else:
            # input not in 'debug, info, error, critical'
            print("level_name 을 다시 입력하세요")
            raise ValueError

    def change_level(self, level):
        """ change logger level """
        self._logger.setLevel(self.set_level(level))

    def check_level(self):
        """
            #1 level = debug , return value = 10
            #2 level = info , return value = 20
            #3 level = error , return value = 30
            #4 level = critical , return value = 40
        """
        print(self._logger.level)

    def print_info(self, msg):
        self._logger.info(msg)

    def print_info_tg(self, msg):
        self._logger.info(msg)
        sendTo('harry', "info: " + msg)

    def print_error(self, msg):
        self._logger.error(msg)

    def print_error_tg(self, msg):
        self._logger.error(msg)
        sendTo('harry', msg)

    def get_logger(self):
        return self._logger


# format_msg = '[%(levelname)s|%(filename)s:%(lineno)s %(asctime)s > %(message)s'
# logger = CustomLogger.__call__('info', format_msg)
# logger.change_level('debug')
# logger.check_level()
# logger.set_streamhandler()
# logger.print_info_tg('here?')