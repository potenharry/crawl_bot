import datetime
import os.path

log_dir = '../logs/'
log_raw_dir = '../logs/raw/'


def store(data_set, data_type):
    cur_date = str(datetime.datetime.now())[:10]
    file_name = data_type + '_' + cur_date
    file_dir = log_dir + file_name + '.txt'
    if os.path.isfile(file_dir):
        file_cnt = 1
        while True:
            file_name = data_type + '_' + cur_date + '_' + str(file_cnt)
            file_dir = log_dir + file_name + '.txt'
            if not os.path.isfile(file_dir):
                break
            file_cnt += 1

    for data in data_set:
        with open(file_dir, 'a') as f:
            f.write(str(data) + '\n')


def store_raw_data(data, data_type):
    cur_date = str(datetime.datetime.now())[:10]
    file_name = data_type + '_' + cur_date
    file_dir = log_raw_dir + file_name + '.txt'

    with open(file_dir, 'a') as f:
        f.write(str(data) + '\n')
