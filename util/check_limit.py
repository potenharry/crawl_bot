import requests
import util.my_logger as log
import util.tgalarm as tg
import time

format_msg = '[%(levelname)s]%(asctime)s > %(message)s'
logger = log.CustomLogger.__call__('info', format_msg)

start_time = time.time()

target_url = 'https://sandbox-apigw.koscom.co.kr/v2/market/stocks/market_code/stock_code/investors'
headers = {'apikey': 'l7xx9bc8fbf9cc9e40eab5211c024a63f36e'}

indv_num = 10
frgn_num = 11
inst_num = 8

'''
    frgn_val, frgn_qty, indv_val, indv_qty, inst_val, inst_qty
'''


def check_api_limit():
    stock_code = '005930'
    market = 'kospi'

    url = target_url.replace('market_code', market).replace('stock_code', stock_code)

    response = requests.post(url, headers=headers)
    # HTTPSConnectionPool(host='sandbox-apigw.koscom.co.kr', port=443): Read timed out. (read timeout=30)
    print(response.headers)
    print(response.json())


# check_api_limit()